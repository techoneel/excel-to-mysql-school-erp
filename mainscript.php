<?php

require 'vendor/autoload.php';

use Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


error_log('-----------------------------------------------------------------------------');
error_log('---------------------- SCRIPT STARTED SUCCESSFULLY --------------------------');
error_log('-----------------------------------------------------------------------------');

try{

    $config_array = parse_ini_file('config.ini');
    
    error_log('--------------------- DATABASE CONNECTION DETAILS ---------------------');
    $db_host = $config_array['DB_HOST'];
    error_log('HOST:'.$db_host);
    $db_port = $config_array['DB_PORT'];
    error_log('PORT:'.$db_port);
    $db_database = $config_array['DB_DATABASE'];
    error_log('DATABASE:'.$db_database);
    $db_username = $config_array['DB_USERNAME'];
    error_log('USERNAME:'.$db_username);
    $db_password = $config_array['DB_PASSWORD'];
    error_log('PASSWORD:'.$db_password);
    error_log('-----------------------------------------------------------------------');
    error_log('Connecting to database...');
    $db_connect = mysqli_connect($db_host,$db_username,$db_password,$db_database);
    error_log('-----------------------------------------------------------------------');
    
    if ($db_connect->connect_errno) {
        printf("Connect failed: %s\n", $db_connect->connect_error);
        error_log('-----------------------------------------------------------------------');
        exit();
    }
    error_log('Connected to database...');
    error_log('-----------------------------------------------------------------------');
    error_log('-------------------------- EXCEL FILE DETAILS -------------------------');
    /* get excel file info */
    $excelFileName = $config_array['EXCEL_FILE'];
    error_log('EXCEL_FILE:'.$excelFileName);
    $excelSheets = $config_array['EXCEL_SHEET'];
    error_log('EXCEL_SHEET:'.print_r($excelSheets,true));
    error_log('-----------------------------------------------------------------------');
    error_log('---------------------- GETTING EXCEL READER READY ---------------------');
    /**  Create a new Reader of the type Xlsx  **/
    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    /** Load $inputFileName to a Spreadsheet Object  **/
    $spreadsheet = $reader->load('excel/'.$excelFileName);
    /* get sheets array */
    $reader->setLoadSheetsOnly([implode(",",$excelSheets)]);
    error_log('-----------------------------------------------------------------------');
    error_log('--------------------- INSERTING CLASS DETAILS DATA --------------------');
    /**  Convert Spreadsheet Object to an Array for ease of use  **/
    $spreadsheet->setActiveSheetIndexByName($excelSheets[0]);
    $classes = $spreadsheet->getActiveSheet()->toArray();
    
    $db_connect->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
    foreach( $classes as $class ){               
        // error_log(print_r($class,true));
        if($classes[0] == $class){
            error_log('-----------------------------------------------------------------------------');
        } else {
            $res_class_details = $db_connect->query("INSERT INTO t_class_details (class, section, active_status) VALUES ('".$class[0]."','".$class[1]."','".$class[2]."')");
            if ( ! $res_class_details ) {
                error_log('Class data not updated!');
                error_log('-----------------------------------------------------------------------------');
                throw new Exception($db_connect->error);
            }
        }
    }
    $class_data_result = $db_connect->commit();
    error_log('Class Data Insert Result:'.$class_data_result);
    error_log('-----------------------------------------------------------------------------');
    
    if(!$class_data_result) {
        error_log('Class data not updated!');
        error_log('-----------------------------------------------------------------------------');
        throw new Exception($db_connect->error);
    }
    error_log('Class data updated successfully!');
    error_log('-----------------------------------------------------------------------------');
    error_log('---------------------- INSERTING TEACHERS DETAILS DATA ----------------------');
    /**  Convert Spreadsheet Object to an Array for ease of use  **/
    $spreadsheet->setActiveSheetIndexByName($excelSheets[1]);
    $teachers = $spreadsheet->getActiveSheet()->toArray();
    
    $db_connect->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
    foreach( $teachers as $teacher ){               
        // error_log(print_r($teacher,true));
        if($teachers[0] == $teacher){
            error_log('-----------------------------------------------------------------------------');
        } else {
    
            /* Date of Birth */
            $time = strtotime(str_replace('.', '/', $teacher[6]));
            $t_dob = date('Y-m-d',$time);
    
            $username = strtolower($teacher[1]).strtolower($teacher[3]);
            $password = 'password';
            $teacher_master_sql = "INSERT INTO t_teacher_master (teacher_first_name, teacher_middle_name, teacher_surname, teacher_dob) VALUES ('".$teacher[1]."','".$teacher[2]."','".$teacher[3]."','".$t_dob."')";
            // error_log('teacher_master_sql:'.$teacher_master_sql);
            $res_t_teacher_master = $db_connect->query($teacher_master_sql);
            if ( ! $res_t_teacher_master ) {
                throw new Exception($db_connect->error);
            }
            $teacher_id = $db_connect->insert_id;

            $relation_class_subject_teacher_sql = "INSERT INTO relation_class_subject_teacher (user_name,password,class,section,teacher_id,teacher_first_name,teacher_middle_name,teacher_surname,class_teacher,all_class_permission,academic_session_id,active_status,temporary_password) VALUES ('".$username."','".$password."','".$teacher[13]."','".$teacher[14]."','".$teacher_id."','".$teacher[1]."','".$teacher[2]."','".$teacher[3]."','1','0','2','1','".$password."')";
            // error_log('relation_class_subject_teacher_sql:'.$relation_class_subject_teacher_sql);
            $res_relation_class_subject_teacher = $db_connect->query($relation_class_subject_teacher_sql);
            if ( ! $res_relation_class_subject_teacher ) {
                throw new Exception($db_connect->error);
            }
        }               
    }
    $teachers_data_result = $db_connect->commit();
    error_log('Teachers Data Insert Result:'.$teachers_data_result);
    error_log('-----------------------------------------------------------------------------');
    if(!$teachers_data_result) {
        error_log('Teachers data not updated!');
        error_log('-----------------------------------------------------------------------------');
        throw new Exception($db_connect->error);
    }
    error_log('Teachers data updated successfully!');
    error_log('-----------------------------------------------------------------------------');
    error_log('---------------------- INSERTING STUDENTS DETAILS DATA ----------------------');
    /**  Convert Spreadsheet Object to an Array for ease of use  **/
    $spreadsheet->setActiveSheetIndexByName($excelSheets[2]);
    $students = $spreadsheet->getActiveSheet()->toArray();
    
    $db_connect->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
    foreach( $students as $student ){
        // error_log(print_r($student,true));
        if($students[0] == $student){
            error_log('-----------------------------------------------------------------------------');
        } else {
            $admission_number = $student[0];

            $name_str = $student[1];
            $pos = strripos($name_str,' ');
            $first_name = substr($name_str, 0, $pos);
            $last_name = substr($name_str, $pos+1);

            $roll = $student[2];

            $class_sec_str = $student[3];
            $class = substr($class_sec_str, 0, strpos($class_sec_str, ' '));
            $section = substr($class_sec_str, strpos($class_sec_str, ' ')+1);

            /* Date of Birth */
            $time = strtotime(str_replace('.', '/', $student[4]));
            $s_dob = date('Y-m-d',$time);


            $father_name = $student[5];
            $address = $student[6];

            $phone_str = $student[7];
            if(!strstr($phone_str,'/')){
                $phone = $phone_str;
                $phone_alt = $phone_str;
            } else {
                $phones = explode('/',$phone_str);
                $phone = $phones[0];
                $phone_alt = $phones[1];
            }

            $student_master_sql='INSERT INTO t_student_master (roll_no,adm_reg_no,student_first_name,student_surname,student_dob,father_name,phone_residence,cell_father,address) VALUES ("'.$roll.'","'.$admission_number.'","'.$first_name.'","'.$last_name.'","'.$s_dob.'","'.$father_name.'","'.$phone_alt.'","'.$phone.'","'.$address.'")';
            // error_log('student_master_sql:'.$student_master_sql);
            $res_student_master = $db_connect->query($student_master_sql);
            if ( ! $res_student_master ) {  // testing for FALSE is only safe way
                throw new Exception($db_connect->error);
            }
            $student_id = $db_connect->insert_id;
            // error_log('Student ID:'.$student_id);

            $student_details_sql = "INSERT INTO t_student_details (class, section, roll_no, year_period, active_status, student_id) VALUES ('".$class."','".$section."','".$roll."','2','1','".$student_id."')";
            $res_student_details = $db_connect->query($student_details_sql);
            if ( ! $res_student_details ) {  // testing for FALSE is only safe way
                throw new Exception($db_connect->error);
            }
        }
    }
    $students_data_result = $db_connect->commit();
    error_log('Students Data Insert Result:'.$students_data_result);
    error_log('-----------------------------------------------------------------------------');
    if(!$students_data_result) {
        error_log('Students data not updated!');
        error_log('-----------------------------------------------------------------------------');
        throw new Exception($db_connect->error);
    }
    error_log('Students data updated successfully!');
    error_log('-----------------------------------------------------------------------------');
} catch(Exception $ex) {
    error_log('Exception:'.$ex->getMessage());
    $db_connect->rollback();
} finally {
    if($db_connect){
        $db_connect->close();
    }
}
error_log('-----------------------------------------------------------------------------');
error_log('---------------------- SCRIPT ENDED SUCCESSFULLY ----------------------------');
error_log('-----------------------------------------------------------------------------');

