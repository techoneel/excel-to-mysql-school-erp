 # Excel to MySQL Database Script

>
> - ## Files Information:
>   1. ### **config.ini :** This file is consist of the information below:
>       - DB_HOST: Hostname or ip of your MySQL database server
>       - DB_PORT: Port of your MySQL database server
>       - DB_DATABASE: Database Name of your MySQL database server
>       - DB_USERNAME: Username of your MySQL database server
>       - DB_PASSWORD: Password of your MySQL database server
>       - EXCEL_FILE: Name of the excel file located at ***excel*** folder
>       - EXCEL_SHEET: Name of the sheets of the excel file
>   2. ### **table.sql :** This is the updated script file inside **sql** folder for the existing excel file.
>   3. ### **data.xlsx :** The excel file inside excel folder consists of 3 sheets and those are:
>         - class-section: Class & Section Data
>         - teachers: Teachers Data
>         - students: Students Data
>   4. ### **mainscript.php :** The main php script which will read the excel and update the database.

> - ## Requirements:
>   - **PHP Version :** 7.3
>   - **Composer Version :** 1.6.3
>   - **MySQL Version :** 5.7

> - ## Perform the following steps:
>     - ### **Step 1 :** Update the database by running the sql script.
>        1. Drop the tables mentioned below:
>             - t_class_details
>             - t_teacher_master
>             - relation_class_subject_teacher
>             - t_student_master
>             - t_student_details
>         2. Run the sql/table.sql script.
>     - ### **Step 2 :** Update the configuration file's [*config.ini*] info that mentioned below:
>          - DB_HOST
>          - DB_PORT
>          - DB_DATABASE
>          - DB_USERNAME
>          - DB_PASSWORD
> 
>     - ### **Step 3 :** Run the php script mainscript.php
>          - open this folder in the terminal
>          - run the below command
>            >`php mainscript.php ↩`
