-- ############################
-- UPDATED TABLE STRUCTURE
-- ############################

-- --------------------------------------------------------
-- Table structure for table `t_student_master`
-- --------------------------------------------------------
CREATE TABLE `t_student_master` (
  `student_id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `roll_no` varchar(100) DEFAULT NULL,
  `adm_reg_no` varchar(20) DEFAULT NULL,
  `adm_reg_date` datetime DEFAULT NULL,
  `student_first_name` varchar(200) DEFAULT NULL,
  `student_surname` varchar(200) DEFAULT NULL,
  `student_dob` datetime DEFAULT NULL,
  `student_gender` varchar(10) DEFAULT NULL,
  `student_category` varchar(10) DEFAULT NULL,
  `student_blood_gr` varchar(10) DEFAULT NULL,
  `aadhar_no` varchar(50) DEFAULT NULL,
  `father_name` varchar(200) DEFAULT NULL,
  `mother_name` varchar(200) DEFAULT NULL,
  `phone_residence` varchar(50) DEFAULT NULL,
  `phone_office` varchar(50) DEFAULT NULL,
  `cell_father` varchar(50) DEFAULT NULL,
  `cell_mother` varchar(50) DEFAULT NULL,
  `primary_email_id` varchar(200) DEFAULT NULL,
  `secondary_email_id` varchar(200) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `district` varchar(200) DEFAULT NULL,
  `pin` int(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
-- Table structure for table `t_student_details`
-- --------------------------------------------------------
CREATE TABLE `t_student_details` (
  `stu_dtl_id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `class` varchar(100) DEFAULT NULL,
  `section` varchar(100) DEFAULT NULL,
  `roll_no` varchar(100) DEFAULT NULL,
  `year_period` varchar(100) DEFAULT NULL,
  `active_status` int(1) DEFAULT NULL COMMENT '0-Deactive 1-Active',
  `student_id` int(10) DEFAULT NULL,
  `halfyearly_remark` varchar(500) DEFAULT NULL,
  `annually_remark` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
-- Table structure for table `t_class_details`
-- --------------------------------------------------------
CREATE TABLE `t_class_details` (
  `class_id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `class` varchar(100) DEFAULT NULL,
  `section` varchar(100) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `active_status` int(1) DEFAULT NULL COMMENT '0-inactive 1-active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
-- Table structure for table `t_teacher_master`
-- --------------------------------------------------------
CREATE TABLE `t_teacher_master` (
  `teacher_id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `teacher_first_name` varchar(200) DEFAULT NULL,
  `teacher_middle_name` varchar(100) DEFAULT NULL,
  `teacher_surname` varchar(200) DEFAULT NULL,
  `teacher_dob` datetime DEFAULT NULL,
  `img_signature` varchar(1000) DEFAULT NULL,
  `degree_certificate` varchar(500) DEFAULT NULL,
  `degree_certificate_img_path` varchar(1000) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
-- Table structure for table `relation_class_subject_teacher`
-- --------------------------------------------------------
CREATE TABLE `relation_class_subject_teacher` (
  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_name` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `class` varchar(200) DEFAULT NULL,
  `section` varchar(50) DEFAULT NULL,
  `teacher_id` int(10) DEFAULT NULL,
  `teacher_first_name` varchar(200) DEFAULT NULL,
  `teacher_middle_name` varchar(100) DEFAULT NULL,
  `teacher_surname` varchar(200) DEFAULT NULL,
  `subject_id` int(10) DEFAULT NULL,
  `subject_name` varchar(200) DEFAULT NULL,
  `sl_no` int(5) DEFAULT NULL,
  `class_teacher` int(1) DEFAULT NULL COMMENT '1-yes,0-no',
  `all_class_permission` int(1) DEFAULT NULL COMMENT '0-No 1-Yes',
  `academic_session_id` int(10) DEFAULT NULL,
  `month` varchar(100) DEFAULT NULL,
  `active_status` int(1) DEFAULT NULL COMMENT '0-Deactive 1-Active',
  `temporary_password` varchar(100) DEFAULT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

